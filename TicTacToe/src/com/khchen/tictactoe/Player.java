package com.khchen.tictactoe;


/**
 * This is a Player interface.
 * @author 	David
 * @version 1.0
 * @since	2015-01-25
 */
public interface Player {
	public int getPlayerNumber();
	
	public void setPlayerNumber(int num);
	
	public boolean isHumanPlayer();
	
	public Move nextMove(TicTacToeModel gameBoard);
	
	public Player makeCopy();
}
