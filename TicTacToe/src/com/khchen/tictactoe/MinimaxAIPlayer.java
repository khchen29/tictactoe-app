package com.khchen.tictactoe;

import java.util.LinkedList;

/**
 * This class represents an AI player implemented using Mini-max 
 * decision rule.
 * @author 	David
 * @version 1.0
 * @since	2015-01-25
 */
public class MinimaxAIPlayer implements Player {
	private int playerNumber;
	private int maxDepthLevel = 4;		//max depth of the decision search
	
	/**
     * This method tells if this class represents a human player.
     * @param None.
     * @return boolean.
     */
	public boolean isHumanPlayer() {
		return false;
	}
	
	/**
     * This method makes a deep copy of this object.
     * @param None.
     * @return MinimaxAIPlayer.
     */
	public MinimaxAIPlayer makeCopy() {
		MinimaxAIPlayer newPlayer = new MinimaxAIPlayer();
		newPlayer.setPlayerNumber(this.playerNumber);
		return newPlayer;
	}
	
	/**
     * This method returns the next best move for the game.
     * @param gameBoard This is the current state of the game board.
     * @return Move This is the next best move.
     */
	public Move nextMove(TicTacToeModel gameBoard) {
		if(playerNumber==1) {
			return findBestMovePlayer1(gameBoard, Integer.MIN_VALUE, Integer.MAX_VALUE, 1);
		} else {
			return findBestMovePlayer2(gameBoard, Integer.MIN_VALUE, Integer.MAX_VALUE, 1);
		}
	}

	public int getPlayerNumber() {
		return playerNumber;
	}

	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}
	
	/**
     * This method simulates the game and finds a best move for player 1.
     * The higher the estimated score is, the better for player 1 to win.
     * @param gameBoard This is the current state of the game board.
     * @param depthLevel The current depth of decision search.
     * @return Move This is the calculated best move.
     */
	private Move findBestMovePlayer1(TicTacToeModel gameBoard, int alpha, int beta, int depthLevel) {
		//check if game is over or max depth is achieved
		int winnerStatus = gameBoard.checkWinner();
		if(winnerStatus==1 || winnerStatus==2 || winnerStatus==-1 || depthLevel>maxDepthLevel) {
			Move move = new Move();
			move.setScore(analyzeBoard(gameBoard));
			System.out.println("Player 1" + move.getScore());
			return move;
		}

		Move bestMove = new Move();
		bestMove.setScore(Integer.MIN_VALUE);		//For player1, the higher score the better.
		
		LinkedList<Move> legalMoves = findLegalMoves(gameBoard);
		// search the best move within the legal moves
		for(int i=0; i<legalMoves.size(); i++) {
			Move newMove = legalMoves.get(i);
			newMove.setPlayerNumber(1);
			//copy game board
			TicTacToeModel newGameBoard = new TicTacToeModel(gameBoard);
			newGameBoard.setBoardValue(newMove);
			newGameBoard.toggleCurrentPlayer();
			//estimate opponent's best move
			Move tempMove = findBestMovePlayer2(newGameBoard, alpha, beta, depthLevel+1);
			//update the best move if needed
			if(tempMove.getScore()>bestMove.getScore()) {
				bestMove.setScore(tempMove.getScore());
				bestMove.setRow(newMove.getRow());
				bestMove.setCol(newMove.getCol());
				// update alpha score
				alpha = tempMove.getScore();
			}
			// check for pruning
			if(alpha >= beta) {
				break;
			}
					
		}

		return bestMove;
	}
	
	/**
     * This method simulates the game and finds a best move for player 2.
     * The lower the estimated score is, the better for player 2 to win.
     * @param gameBoard This is the current state of the game board.
     * @param depthLevel The current depth of decision search.
     * @return Move This is the calculated best move.
     */
	private Move findBestMovePlayer2(TicTacToeModel gameBoard, int alpha, int beta, int depthLevel) {
		//check if game is over or max depth is achieved
		int winnerStatus = gameBoard.checkWinner();
		if(winnerStatus==1 || winnerStatus==2 || winnerStatus==-1 || depthLevel>maxDepthLevel) {
			Move move = new Move();
			move.setScore(analyzeBoard(gameBoard));
			System.out.println("Player 2" + move.getScore());
			return move;
		}

		Move bestMove = new Move();
		bestMove.setScore(Integer.MAX_VALUE);		//For player2, the lower score the better.
		
		LinkedList<Move> legalMoves = findLegalMoves(gameBoard);
		// search the best move within the legal moves
		for(int i=0; i<legalMoves.size(); i++) {
			Move newMove = legalMoves.get(i);
			newMove.setPlayerNumber(2);
			//copy game board
			TicTacToeModel newGameBoard = new TicTacToeModel(gameBoard);
			newGameBoard.setBoardValue(newMove);
			newGameBoard.toggleCurrentPlayer();
			//estimate opponent's best move
			Move tempMove = findBestMovePlayer1(newGameBoard, alpha, beta, depthLevel+1);
			//update the best move if needed
			if(tempMove.getScore()<bestMove.getScore()) {
				bestMove.setScore(tempMove.getScore());
				bestMove.setRow(newMove.getRow());
				bestMove.setCol(newMove.getCol());
				// update beta score
				beta = tempMove.getScore();
			}
			// check for pruning
			if(alpha >= beta) {
				break;
			}
		}

		return bestMove;
	}
	
	/**
     * This method calculates the score of the current game board state. 
     * @param gameBoard This is the current state of the game board.
     * @return int The score of the current game board state.
     */
	private int analyzeBoard(TicTacToeModel gameBoard) {
		int winnerStatus = gameBoard.checkWinner();
		// player 1 wins
		if(winnerStatus==1) {
			return 1000;
		// player 2 wins
		} else if(winnerStatus==2) {
			return -1000;
		// tie game
		} else if(winnerStatus==-1) {
			return 0;
		// middle of game
		} else {
			int totalScore = 0;
			for(int i=0; i<3; i++) {
				for(int j=0; j<3; j++) {
					totalScore += lookupScoreValue(i, j, gameBoard.getBoardValue(i, j));
				}
			}
			return totalScore;
		}
	}
	
	/**
     * This method returns a score value at a given game board location. The 
     * middle of the board weights the most (100 pts). Then, the four corners
     * of the board (20 pts). Finally, the remaining sides (10 pts).
     * @param row Row index of the game board.
     * @param col Column index of the game board.
     * @param playerNumber.
     * @return int The score of the given game board location.
     */
	private int lookupScoreValue(int row, int col, int playerNumber) {
		// empty space
		if(playerNumber==0) {
			return 0;
		}
		int score = 0;
		// middle
		if(row==1 && col==1) {
			score = 100;
		// four corners
		} else if((row==0 && col==0) || (row==0 && col==2) ||
				  (row==2 && col==0) || (row==2 && col==2)) {
			score = 20;
		//sides
		} else if((row==0 && col==1) || (row==2 && col==1) ||
				  (row==1 && col==0) || (row==1 && col==2)) {
			score = 10;
		}
		
		// player 1: positive value | player 2: negative value
		if(playerNumber==2) {
			score = -1*score;
		}
		return score;
	}
	
	/**
     * This method returns a list of legal moves for the current player.
     * @param gameBoard This is the current state of the game board.
     * @return LinkedList<Move> A list of legal moves.
     */
	private LinkedList<Move> findLegalMoves(TicTacToeModel gameBoard) {
		LinkedList<Move> moves = new LinkedList<Move>();
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++) {
				if(gameBoard.getBoardValue(i, j)==0) {
					moves.add(new Move(i, j));
				}
			}
		}
		return moves;
	}

}
