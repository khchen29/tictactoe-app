package com.khchen.tictactoe;

/**
 * This class implements a Tic Tac Toe game board.
 * @author 	David
 * @version 1.0
 * @since	2015-01-25
 */
public class TicTacToeModel extends GameBoardModel {
	
	/**
	 * Default constructor.
	 */
	public TicTacToeModel() {
		super();
	}
	
	/**
	 * Constructor with parameters.
	 */
	public TicTacToeModel(Player player1, Player player2) {
		super(3, 3, player1, player2);
	}
	
	/**
	 * Copy constructor.
	 */
	public TicTacToeModel(TicTacToeModel gameBoard) {
		this.board = gameBoard.makeBoardCopy();
		this.player1 = gameBoard.getPlayer1().makeCopy();
		this.player2 = gameBoard.getPlayer2().makeCopy();
		if(gameBoard.getCurrentPlayer().getPlayerNumber()==1) {
			this.currentPlayer = this.player1;
		} else {
			this.currentPlayer = this.player2;
		}
	}
	
	/**
     * This method checks the current game board and determines if there
     * is a winner or it's a tie game.
     * @param None.
     * @return int If player 1 wins, returns 1. If player 2 wins, returns 2.
     * 			If it's a tie game, returns -1. If none of the above, returns 0.
     */
	public int checkWinner() {
		//check rows
		for (int i=0; i<3; i++) {
			if (board[i][0]!=0 && board[i][0]==board[i][1] && board[i][1]==board[i][2]) {
				return board[i][0];
			}
		}
		//check columns
		for (int i=0; i<3; i++) {
			if (board[0][i]!=0 && board[0][i]==board[1][i] && board[1][i]==board[2][i]) {
				return board[0][i];
			}
		}
		//check diagonals
		if (board[0][0]!=0 && board[0][0]==board[1][1] && board[1][1]==board[2][2]) {
			return board[0][0];
		} else if (board[0][2]!=0 && board[0][2]==board[1][1] && board[1][1]==board[2][0]) {
			return board[0][2];
		}

		//check for tie condition. If there is a tie, returns -1
		for (int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				if (board[i][j]==0) {
					return 0;
				}
			}
		}
		return -1;
	}
}
