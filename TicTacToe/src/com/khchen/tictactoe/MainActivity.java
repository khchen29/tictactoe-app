package com.khchen.tictactoe;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements OnClickListener {
	
	final int boardHeight = 3;
	final int boardWidth = 3;
	Button[][] gameBoardButtons;
	Button newGameButton;
	TextView gameResultText;
	TicTacToeModel gameBoard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // register text views
        gameResultText = (TextView) findViewById(R.id.textView_GameResult);
        
        // register button events
        gameBoardButtons = new Button[boardHeight][boardWidth];
        registerButtons();
                
        // initialize game board
        Player player1 = new HumanPlayer();
        //Player player2 = new HumanPlayer();
        //Player player1 = new MinimaxAIPlayer();
        Player player2 = new MinimaxAIPlayer();
        gameBoard = new TicTacToeModel(player1, player2);
        
        // run next move automatically if player1 is not a human player
		if(!gameBoard.getCurrentPlayer().isHumanPlayer()) {
			executeNextMove(gameBoard);
		}
    }
    
    /**
     * This is the onClick event handler for the buttons.
     * @param view This is the button object that is clicked.
     * @return None.
     */
    @Override
    public void onClick(View view) {
    	Button button = (Button) view;
    	// "New Game" button is clicked
    	if(button.getId()==R.id.button_NewGame) {
    		resetGame();
    		if(!gameBoard.getCurrentPlayer().isHumanPlayer()) {
    			executeNextMove(gameBoard);
    		}
    	} else {
	    	// update game board
	    	Move move = findButtonLocation(button);
	    	move.setPlayerNumber(gameBoard.getCurrentPlayer().getPlayerNumber());
	    	gameBoard.setBoardValue(move);
	    	if(gameBoard.getCurrentPlayer().getPlayerNumber()==1) {
	    		button.setText("X");
	    	} else {
	    		button.setText("O");
	    	}
	    	button.setClickable(false);
	    	
	    	// check if there is a winner
	    	int winnerValue = gameBoard.checkWinner();
	    	if (winnerValue==1) {
	    		disableGameBoardButtons();
	    		gameResultText.setText(R.string.player1_wins);
	    		gameResultText.setVisibility(View.VISIBLE);
	    	} else if (winnerValue==2) {
	    		disableGameBoardButtons();
	    		gameResultText.setText(R.string.player2_wins);
	    		gameResultText.setVisibility(View.VISIBLE);
	    	} else if (winnerValue==-1) {
	    		gameResultText.setText(R.string.tie_game);
	    		gameResultText.setVisibility(View.VISIBLE);
	    	} else {
	    		// toggle player
	    		gameBoard.toggleCurrentPlayer();
	    		// run next move automatically if it's not a human player
	    		if(!gameBoard.getCurrentPlayer().isHumanPlayer()) {
	    			executeNextMove(gameBoard);
	    		}
	    	}
    	}
    	
    }
    
    /**
     * This method registers all the buttons on screen to a onClick
     * event listener.
     * @param None.
     * @return None.
     */
    private void registerButtons() {
    	//game board buttons
    	gameBoardButtons[0][0] = (Button) findViewById(R.id.button00);
    	gameBoardButtons[0][1] = (Button) findViewById(R.id.button01);
    	gameBoardButtons[0][2] = (Button) findViewById(R.id.button02);
    	gameBoardButtons[1][0] = (Button) findViewById(R.id.button10);
    	gameBoardButtons[1][1] = (Button) findViewById(R.id.button11);
    	gameBoardButtons[1][2] = (Button) findViewById(R.id.button12);
    	gameBoardButtons[2][0] = (Button) findViewById(R.id.button20);
    	gameBoardButtons[2][1] = (Button) findViewById(R.id.button21);
    	gameBoardButtons[2][2] = (Button) findViewById(R.id.button22);
    	for (int i=0; i<boardHeight; i++) {
    		for (int j=0; j<boardWidth; j++) {
    			gameBoardButtons[i][j].setOnClickListener(this);
    		}    		
    	}
    	//new game button
    	newGameButton = (Button) findViewById(R.id.button_NewGame);
    	newGameButton.setOnClickListener(this);
    }
    
    /**
     * This method finds out which button on the game board was clicked
     * and returns its location axis.
     * @param b This is the button object that was clicked on the game board.
     * @return Move This object contains the row and column index of the
     * 			given button on the game board.
     */
    private Move findButtonLocation(Button b) {
    	Move move;
    	if (b.getId() == R.id.button00) {
    		move = new Move(0,0);
    	} else if (b.getId() == R.id.button01) {
    		move = new Move(0,1);
    	} else if (b.getId() == R.id.button02) {
    		move = new Move(0,2);
    	} else if (b.getId() == R.id.button10) {
    		move = new Move(1,0);
    	} else if (b.getId() == R.id.button11) {
    		move = new Move(1,1);
    	} else if (b.getId() == R.id.button12) {
    		move = new Move(1,2);
    	} else if (b.getId() == R.id.button20) {
    		move = new Move(2,0);
    	} else if (b.getId() == R.id.button21) {
    		move = new Move(2,1);
    	} else if (b.getId() == R.id.button22) {
    		move = new Move(2,2);
    	} else {
    		move = new Move();
    	}
    	return move;
    }
    
    /**
     * This method resets the game board to its original state.
     * @param None.
     * @return None.
     */
    private void resetGame() {
    	gameBoard.reset();
    	gameResultText.setVisibility(View.INVISIBLE);
    	for (int i=0; i<boardHeight; i++) {
    		for (int j=0; j<boardWidth; j++) {
    			gameBoardButtons[i][j].setText("");
    			gameBoardButtons[i][j].setClickable(true);
    		}    		
    	}
    }
    
    /**
     * This method disables all the game board buttons. It is called
     * whenever there is a winner to avoid further actions on the game
     * board.
     * @param None.
     * @return None.
     */
    private void disableGameBoardButtons() {
    	for (int i=0; i<boardHeight; i++) {
    		for (int j=0; j<boardWidth; j++) {
    			gameBoardButtons[i][j].setClickable(false);
    		}    		
    	}
    }
    
    /**
     * This method simulates button clicking when it's AI player's turn.
     * @param gameBoard This is the current state of the game board.
     * @return None.
     */
    private void executeNextMove(TicTacToeModel gameBoard) {
    	Move move = gameBoard.getCurrentPlayer().nextMove(gameBoard);
    	if (move.getRow()==0 && move.getCol()==0) {
    		findViewById(R.id.button00).performClick();
    	} else if (move.getRow()==0 && move.getCol()==1) {
    		findViewById(R.id.button01).performClick();
    	} else if (move.getRow()==0 && move.getCol()==2) {
    		findViewById(R.id.button02).performClick();
    	} else if (move.getRow()==1 && move.getCol()==0) {
    		findViewById(R.id.button10).performClick();
    	} else if (move.getRow()==1 && move.getCol()==1) {
    		findViewById(R.id.button11).performClick();
    	} else if (move.getRow()==1 && move.getCol()==2) {
    		findViewById(R.id.button12).performClick();
    	} else if (move.getRow()==2 && move.getCol()==0) {
    		findViewById(R.id.button20).performClick();
    	} else if (move.getRow()==2 && move.getCol()==1) {
    		findViewById(R.id.button21).performClick();
    	} else if (move.getRow()==2 && move.getCol()==2) {
    		findViewById(R.id.button22).performClick();
    	}
    }
}
