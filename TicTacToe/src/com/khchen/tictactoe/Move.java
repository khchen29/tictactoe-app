package com.khchen.tictactoe;

/**
 * This class represents a single move by a player.
 * @author 	David
 * @version 1.0
 * @since	2015-01-25
 */
public class Move {
	private int row;
	private int col;
	private int playerNumber;
	private int score;
	
	/**
	 * Default constructor.
	 */
	public Move() {
		
	}	
	
	/**
	 * Constructor with parameters.
	 */
	public Move(int row, int col) {
		this.setRow(row);
		this.setCol(col);
	}
	
	/**
	 * Constructor with parameters.
	 */
	public Move(int row, int col, int playerNumber) {
		this.setRow(row);
		this.setCol(col);
		this.setPlayerNumber(playerNumber);
	}
	
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getCol() {
		return col;
	}
	public void setCol(int col) {
		this.col = col;
	}
	public int getPlayerNumber() {
		return playerNumber;
	}
	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}	
	
}
