package com.khchen.tictactoe;

/**
 * This class represents a human player.
 * @author 	David
 * @version 1.0
 * @since	2015-01-25
 */
public class HumanPlayer implements Player {
	private int playerNumber;
	
	/**
     * This method tells if this class represents a human player.
     * @param None.
     * @return boolean.
     */
	public boolean isHumanPlayer() {
		return true;
	}
	
	/**
     * This method returns the next best move for the game.
     * @param gameBoard This is the current state of the game board.
     * @return Move This is the next best move.
     */
	public Move nextMove(TicTacToeModel gameBoard) {
		return null;
	}

	/**
     * This method makes a deep copy of this object.
     * @param None.
     * @return HumanPlayer.
     */
	public HumanPlayer makeCopy() {
		HumanPlayer newPlayer = new HumanPlayer();
		newPlayer.setPlayerNumber(this.playerNumber);
		return newPlayer;
	}
	
	public int getPlayerNumber() {
		return playerNumber;
	}

	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}	
}
