package com.khchen.tictactoe;

/**
 * This is an abstract class represents a two-player game board. Any kind of 
 * two-player game can create its class by extending this GameBoardModel class.
 * @author 	David
 * @version 1.0
 * @since	2015-01-25
 */
public abstract class GameBoardModel {
	protected int[][] board;
	protected Player player1;
	protected Player player2;
	// Linklist history?
	protected Player currentPlayer;
	
	/**
	 * Default constructor
	 */
	public GameBoardModel() {
		
	}
	
	/**
     * Constructor with parameters.
     */
	public GameBoardModel(int boardHeight, int boardWidth, Player player1, Player player2) {
		this.board = new int[boardHeight][boardWidth];
		this.player1 = player1;
		this.player1.setPlayerNumber(1);
		this.player2 = player2;
		this.player2.setPlayerNumber(2);
		this.currentPlayer = this.player1;		
	}
	
	/**
     * This method resets the game board to the original state.
     * @param None.
     * @return None.
     */
	public void reset() {
		int height = board.length;
		int width = board[0].length;
		board = new int[height][width];
		currentPlayer = player1;
	}
	
	/**
     * This method updates the game board by one move.
     * @param move This is a move made by a player.
     * @return None.
     */
	public void setBoardValue(Move move) {
		board[move.getRow()][move.getCol()] = move.getPlayerNumber();
	}
	
	/**
     * This method gets the current game board value at a specified location.
     * @param row Row index of the game board.
     * @param col Column index of the game board.
     * @return int This is a player number (1 or 2) or empty (0).
     */
	public int getBoardValue(int row, int col) {
		return board[row][col];
	}
	
	/**
     * This method makes a deep copy of the game board.
     * @param None.
     * @return int[][] An duplicated object of the current game board.
     */
	public int[][] makeBoardCopy() {
		int height = board.length;
		int width = board[0].length;
		int[][] boardCopy = new int[height][width];
		for (int i=0; i<height; i++) {
			for (int j=0; j<width; j++) {
				boardCopy[i][j] = board[i][j];
			}
		}
		return boardCopy;
	}
	
	/**
     * This method switches the current player from one to another.
     * @param None.
     * @return None.
     */
	public void toggleCurrentPlayer() {
		if(currentPlayer.getPlayerNumber()==1) {
			currentPlayer = player2;
		} else {
			currentPlayer = player1;
		}
	}
	
	/**
     * This method checks the current game board and determines if there
     * is a winner or it's a tie game.
     * @param None.
     * @return int.
     */
	public abstract int checkWinner();

	public int[][] getBoard() {
		return board;
	}

	public void setBoard(int[][] board) {
		this.board = board;
	}

	public Player getPlayer1() {
		return player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
		
}
