****************************************************
	Android App - Tic Tac Toe Game
****************************************************

----------------
What is inside?
----------------
The directory includes two projects.
1. TicTacToe -
	This project is a Tic Tac Toe game on a android
	mobile device. The game opponent (player 2) is
	driven by an AI bot, which is implemented using
	minimax decision rule with a depth search of 2.
2. TicTacToeTest -
	This is a JUnit test project for unit-testing 
	the TicTacToe project.


-------
Updates
-------
07/06/2015	Added alpha-beta pruning to improve 
		search efficiency for the minimax AI
		player.


---------------------
Possible Improvements
---------------------
1. Improve the game UI.
2. Add a game replay functionality.
