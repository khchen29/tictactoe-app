package com.khchen.tictactoe.test;

import static org.junit.Assert.*;

import com.khchen.tictactoe.*;

import org.junit.Test;

public class MinimaxAIPlayerTest {

	@Test
	public void testIsHumanPlayer() {
		Player player = new MinimaxAIPlayer();
		assertFalse(player.isHumanPlayer());
	}
	
	@Test
	public void testMakeCopy() {
		Player player = new MinimaxAIPlayer();
		player.setPlayerNumber(1);
		
		Player playerCopy = player.makeCopy();
		assertNotSame(player, playerCopy);
		assertEquals(player.getPlayerNumber(), playerCopy.getPlayerNumber());
	}
	
	
	/**
	 * 	Test Scenario 1: 
	 * 		0 | 0 | 0
	 * 		----------
	 * 		0 | 0 | 0
	 * 		----------
	 * 		0 | 0 | 0
	 * 		Situation: A new game
	 * 		Whose Turn: player 1
	 * 		Best Move: (1, 1)
	 */
	@Test
	public void testNextMoveScenario1() {
		// initialize board
		Player player1 = new MinimaxAIPlayer();
		player1.setPlayerNumber(1);
		Player player2 = new MinimaxAIPlayer();
		player2.setPlayerNumber(2);
		TicTacToeModel board = new TicTacToeModel(player1, player2);
		
		// verify move
		Move bestMove = player1.nextMove(board);
		assertEquals(1, bestMove.getRow());
		assertEquals(1, bestMove.getCol());
	}
	
	/**
	 * 	Test Scenario 2: 
	 * 		0 | 0 | 0
	 * 		----------
	 * 		0 | 1 | 0
	 * 		----------
	 * 		0 | 0 | 0
	 * 		Situation: Player 1 makes one move in the middle of the board
	 * 		Whose Turn: player 2
	 * 		Best Move: (0, 0). If the middle position is occupied, the next ideal position is 
	 * 					one of the four corners.
	 */
	@Test
	public void testNextMoveScenario2() {
		// initialize board
		Player player1 = new MinimaxAIPlayer();
		player1.setPlayerNumber(1);
		Player player2 = new MinimaxAIPlayer();
		player2.setPlayerNumber(2);
		TicTacToeModel board = new TicTacToeModel(player1, player2);
		board.setBoardValue(new Move(1, 1, 1));
		board.toggleCurrentPlayer();
		
		// verify move
		Move bestMove = player2.nextMove(board);
		assertEquals(0, bestMove.getRow());
		assertEquals(0, bestMove.getCol());
	}
	
	/**
	 * 	Test Scenario 3: 
	 * 		1 | 2 | 0
	 * 		----------
	 * 		0 | 1 | 0
	 * 		----------
	 * 		0 | 0 | 0
	 * 		Situation: Player 1 is about to win
	 * 		Whose Turn: player 2
	 * 		Best Move: (2, 2)
	 */
	@Test
	public void testNextMoveScenario3() {
		// initialize board
		Player player1 = new MinimaxAIPlayer();
		player1.setPlayerNumber(1);
		Player player2 = new MinimaxAIPlayer();
		player2.setPlayerNumber(2);
		TicTacToeModel board = new TicTacToeModel(player1, player2);
		board.setBoardValue(new Move(0, 0, 1));
		board.toggleCurrentPlayer();
		board.setBoardValue(new Move(0, 1, 2));
		board.toggleCurrentPlayer();
		board.setBoardValue(new Move(1, 1, 1));
		board.toggleCurrentPlayer();
		
		// verify move
		Move bestMove = player2.nextMove(board);
		assertEquals(2, bestMove.getRow());
		assertEquals(2, bestMove.getCol());
	}
	
	/**
	 * 	Test Scenario 4: 
	 * 		1 | 0 | 0
	 * 		----------
	 * 		0 | 1 | 0
	 * 		----------
	 * 		2 | 0 | 2
	 * 		Situation: Player 2 is about to win
	 * 		Whose Turn: player 1
	 * 		Best Move: (2, 1)
	 */
	@Test
	public void testNextMoveScenario4() {
		// initialize board
		Player player1 = new MinimaxAIPlayer();
		player1.setPlayerNumber(1);
		Player player2 = new MinimaxAIPlayer();
		player2.setPlayerNumber(2);
		TicTacToeModel board = new TicTacToeModel(player1, player2);
		board.setBoardValue(new Move(0, 0, 1));
		board.toggleCurrentPlayer();
		board.setBoardValue(new Move(2, 0, 2));
		board.toggleCurrentPlayer();
		board.setBoardValue(new Move(1, 1, 1));
		board.toggleCurrentPlayer();
		board.setBoardValue(new Move(2, 2, 2));
		board.toggleCurrentPlayer();
		
		// verify move
		Move bestMove = player1.nextMove(board);
		assertEquals(2, bestMove.getRow());
		assertEquals(1, bestMove.getCol());
	}
	
	/**
	 * 	Test Scenario 5: 
	 * 		1 | 0 | 2
	 * 		----------
	 * 		1 | 0 | 2
	 * 		----------
	 * 		0 | 0 | 0
	 * 		Situation: Player 1 is about to win
	 * 		Whose Turn: player 1
	 * 		Best Move: (2, 0)
	 */
	@Test
	public void testNextMoveScenario5() {
		// initialize board
		Player player1 = new MinimaxAIPlayer();
		player1.setPlayerNumber(1);
		Player player2 = new MinimaxAIPlayer();
		player2.setPlayerNumber(2);
		TicTacToeModel board = new TicTacToeModel(player1, player2);
		board.setBoardValue(new Move(0, 0, 1));
		board.toggleCurrentPlayer();
		board.setBoardValue(new Move(0, 2, 2));
		board.toggleCurrentPlayer();
		board.setBoardValue(new Move(1, 0, 1));
		board.toggleCurrentPlayer();
		board.setBoardValue(new Move(1, 2, 2));
		board.toggleCurrentPlayer();
		
		// verify move
		Move bestMove = player1.nextMove(board);
		assertEquals(2, bestMove.getRow());
		assertEquals(0, bestMove.getCol());
	}

}
