package com.khchen.tictactoe.test;

import static org.junit.Assert.*;

import com.khchen.tictactoe.*;

import org.junit.Test;

public class MoveTest {
	
	// test constructor: public Move(int row, int col)
	@Test
	public void testConstructor1() {
		Move move = new Move(1, 2);
		assertEquals(1, move.getRow());
		assertEquals(2, move.getCol());
	}
	
	// test constructor: public Move(int row, int col, int playerNumber)
	@Test
	public void testConstructor2() {
		Move move = new Move(0, 1, 2);
		assertEquals(0, move.getRow());
		assertEquals(1, move.getCol());
		assertEquals(2, move.getPlayerNumber());
	}

}
