package com.khchen.tictactoe.test;

import static org.junit.Assert.*;

import com.khchen.tictactoe.*;

import org.junit.Test;

public class TicTacToeModelTest {

	@Test
	public void testCopyConstructor() {
		// initialize an original board
		Player player1 = new HumanPlayer();
		player1.setPlayerNumber(1);
		Player player2 = new MinimaxAIPlayer();
		player2.setPlayerNumber(2);
		TicTacToeModel board = new TicTacToeModel(player1, player2);
		board.setBoardValue(new Move(1, 1, 2));
		board.toggleCurrentPlayer();
		
		// make a board copy
		TicTacToeModel newBoard = new TicTacToeModel(board);
		
		// verify the copy result
		assertNotSame(board, newBoard);
		assertNotSame(board.getBoard(), newBoard.getBoard());
		assertNotSame(board.getPlayer1(), newBoard.getPlayer1());
		assertNotSame(board.getPlayer2(), newBoard.getPlayer2());
		
		assertArrayEquals(board.getBoard(), newBoard.getBoard());
		assertEquals(1, newBoard.getPlayer1().getPlayerNumber());
		assertEquals(true, newBoard.getPlayer1().isHumanPlayer());
		assertEquals(2, newBoard.getPlayer2().getPlayerNumber());
		assertEquals(false, newBoard.getPlayer2().isHumanPlayer());
		assertSame(newBoard.getCurrentPlayer(), newBoard.getPlayer2());
	}
	
	@Test
	public void testRest() {
		// initialize board
		int[][] emptyBoard = new int[3][3];
		TicTacToeModel board = new TicTacToeModel(new HumanPlayer(), new HumanPlayer());
		board.setBoardValue(new Move(1, 1, 2));
		board.toggleCurrentPlayer();
		
		// reset board
		board.reset();
		
		// verify
		assertArrayEquals(emptyBoard, board.getBoard());
		assertSame(board.getCurrentPlayer(), board.getPlayer1());		
	}
	
	@Test
	public void testSetBoardValue() {
		// initialize board
		TicTacToeModel board = new TicTacToeModel(new HumanPlayer(), new HumanPlayer());
		Move move1 = new Move(0, 0, 1);
		Move move2 = new Move(1, 1, 2);
		board.setBoardValue(move1);
		board.setBoardValue(move2);
		
		// verify
		int[][] gameBoard = board.getBoard();
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++) {
				if(i==move1.getRow() && j==move1.getCol()) {
					assertEquals(move1.getPlayerNumber(), gameBoard[i][j]);
				} else if(i==move2.getRow() && j==move2.getCol()) {
					assertEquals(move2.getPlayerNumber(), gameBoard[i][j]);
				} else {
					assertEquals(0, gameBoard[i][j]);
				}
			}
		}
	}
	
	@Test
	public void testGetBoardValue() {
		// initialize board
		TicTacToeModel board = new TicTacToeModel(new HumanPlayer(), new HumanPlayer());
		Move move = new Move(2, 2, 1);
		board.setBoardValue(move);
		
		// verify
		int[][] gameBoard = board.getBoard();
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++) {
				if(i==move.getRow() && j==move.getCol()) {
					assertEquals(move.getPlayerNumber(), gameBoard[i][j]);
				} else {
					assertEquals(0, gameBoard[i][j]);
				}
			}
		}
	}
	
	@Test
	public void testMakeBoardCopy() {
		// initialize board
		TicTacToeModel board = new TicTacToeModel(new HumanPlayer(), new HumanPlayer());
		Move move1 = new Move(0, 0, 1);
		Move move2 = new Move(1, 1, 2);
		board.setBoardValue(move1);
		board.setBoardValue(move2);
		
		// make board copy
		int[][] gameBoardCopy = board.makeBoardCopy();
		
		// verify copy result
		assertNotSame(board.getBoard(), gameBoardCopy);
		assertArrayEquals(board.getBoard(), gameBoardCopy);		
	}
	
	@Test
	public void testToggleCurrentPlayer() {
		// initialize board
		Player player1 = new HumanPlayer();
		player1.setPlayerNumber(1);
		Player player2 = new MinimaxAIPlayer();
		player2.setPlayerNumber(2);
		TicTacToeModel board = new TicTacToeModel(player1, player2);
		
		// toggle
		board.toggleCurrentPlayer();
		
		// verify
		assertSame(board.getCurrentPlayer(), board.getPlayer2());		
	}
	
	@Test
	public void testPlayer1Wins() {
		TicTacToeModel board = new TicTacToeModel(new HumanPlayer(), new HumanPlayer());
		// row condition checks
		board.setBoardValue(new Move(0, 0, 1));
		board.setBoardValue(new Move(0, 1, 1));
		board.setBoardValue(new Move(0, 2, 1));		
		assertEquals(1, board.checkWinner());
		
		board.reset();
		board.setBoardValue(new Move(1, 0, 1));
		board.setBoardValue(new Move(1, 1, 1));
		board.setBoardValue(new Move(1, 2, 1));		
		assertEquals(1, board.checkWinner());
		
		board.reset();
		board.setBoardValue(new Move(2, 0, 1));
		board.setBoardValue(new Move(2, 1, 1));
		board.setBoardValue(new Move(2, 2, 1));		
		assertEquals(1, board.checkWinner());
		
		//column conditional checks
		board.reset();
		board.setBoardValue(new Move(0, 0, 1));
		board.setBoardValue(new Move(1, 0, 1));
		board.setBoardValue(new Move(2, 0, 1));		
		assertEquals(1, board.checkWinner());
		
		board.reset();
		board.setBoardValue(new Move(0, 1, 1));
		board.setBoardValue(new Move(1, 1, 1));
		board.setBoardValue(new Move(2, 1, 1));		
		assertEquals(1, board.checkWinner());
		
		board.reset();
		board.setBoardValue(new Move(0, 2, 1));
		board.setBoardValue(new Move(1, 2, 1));
		board.setBoardValue(new Move(2, 2, 1));		
		assertEquals(1, board.checkWinner());
		
		// diagonal condition checks
		board.reset();
		board.setBoardValue(new Move(0, 0, 1));
		board.setBoardValue(new Move(1, 1, 1));
		board.setBoardValue(new Move(2, 2, 1));		
		assertEquals(1, board.checkWinner());
		
		board.reset();
		board.setBoardValue(new Move(0, 2, 1));
		board.setBoardValue(new Move(1, 1, 1));
		board.setBoardValue(new Move(2, 0, 1));		
		assertEquals(1, board.checkWinner());
	}
	
	@Test
	public void testPlayer2Wins() {
		TicTacToeModel board = new TicTacToeModel(new HumanPlayer(), new HumanPlayer());
		// row condition checks
		board.setBoardValue(new Move(0, 0, 2));
		board.setBoardValue(new Move(0, 1, 2));
		board.setBoardValue(new Move(0, 2, 2));		
		assertEquals(2, board.checkWinner());
		
		board.reset();
		board.setBoardValue(new Move(1, 0, 2));
		board.setBoardValue(new Move(1, 1, 2));
		board.setBoardValue(new Move(1, 2, 2));		
		assertEquals(2, board.checkWinner());
		
		board.reset();
		board.setBoardValue(new Move(2, 0, 2));
		board.setBoardValue(new Move(2, 1, 2));
		board.setBoardValue(new Move(2, 2, 2));		
		assertEquals(2, board.checkWinner());
		
		//column conditional checks
		board.reset();
		board.setBoardValue(new Move(0, 0, 2));
		board.setBoardValue(new Move(1, 0, 2));
		board.setBoardValue(new Move(2, 0, 2));		
		assertEquals(2, board.checkWinner());
		
		board.reset();
		board.setBoardValue(new Move(0, 1, 2));
		board.setBoardValue(new Move(1, 1, 2));
		board.setBoardValue(new Move(2, 1, 2));		
		assertEquals(2, board.checkWinner());
		
		board.reset();
		board.setBoardValue(new Move(0, 2, 2));
		board.setBoardValue(new Move(1, 2, 2));
		board.setBoardValue(new Move(2, 2, 2));		
		assertEquals(2, board.checkWinner());
		
		// diagonal condition checks
		board.reset();
		board.setBoardValue(new Move(0, 0, 2));
		board.setBoardValue(new Move(1, 1, 2));
		board.setBoardValue(new Move(2, 2, 2));		
		assertEquals(2, board.checkWinner());
		
		board.reset();
		board.setBoardValue(new Move(0, 2, 2));
		board.setBoardValue(new Move(1, 1, 2));
		board.setBoardValue(new Move(2, 0, 2));		
		assertEquals(2, board.checkWinner());
	}
	
	@Test
	public void testTieGame() {
		TicTacToeModel board = new TicTacToeModel(new HumanPlayer(), new HumanPlayer());
		board.setBoardValue(new Move(0, 0, 1));
		board.setBoardValue(new Move(0, 1, 2));
		board.setBoardValue(new Move(0, 2, 1));	
		board.setBoardValue(new Move(1, 0, 1));
		board.setBoardValue(new Move(1, 1, 2));
		board.setBoardValue(new Move(1, 2, 1));	
		board.setBoardValue(new Move(2, 0, 2));
		board.setBoardValue(new Move(2, 1, 1));
		board.setBoardValue(new Move(2, 2, 2));	
		assertEquals(-1, board.checkWinner());
	}

}
