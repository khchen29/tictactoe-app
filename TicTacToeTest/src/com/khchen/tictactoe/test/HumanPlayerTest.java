package com.khchen.tictactoe.test;

import static org.junit.Assert.*;

import com.khchen.tictactoe.*;

import org.junit.Test;

public class HumanPlayerTest {

	@Test
	public void testIsHumanPlayer() {
		Player player = new HumanPlayer();
		assertTrue(player.isHumanPlayer());
	}
	
	@Test
	public void testNextMove() {
		Player player = new HumanPlayer();
		assertNull(player.nextMove(new TicTacToeModel()));
	}
	
	@Test
	public void testMakeCopy() {
		Player player = new HumanPlayer();
		player.setPlayerNumber(1);
		
		Player playerCopy = player.makeCopy();
		assertNotSame(player, playerCopy);
		assertEquals(player.getPlayerNumber(), playerCopy.getPlayerNumber());
	}

}
